const int pinInput = 2; // digital 3
const int moisture_level = 1; // the value after the LED goes ON
 
void setup() {
    Serial.begin(9600);
}
 
void SoundState(int state) {
    digitalWrite(12, state);
}
 
void loop() {
    int moisture = digitalRead(pinInput);
 
    Serial.println(moisture);
 
    if(moisture == moisture_level) {
        SoundState(HIGH);
    } else   {
        SoundState(LOW);
    }
    delay(1000);
}
